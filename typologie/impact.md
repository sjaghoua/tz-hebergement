# Typologie des impact

## A

impact dramatiques et irréversibles

## B

impact forte mais réversible 

## C 

impact faible mais néanmoins gênante

## X (vide)

impact négligeable voir inexistante
