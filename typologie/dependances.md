# Typologie des dépendances:


## Type de dépendances

### Humain

les dépendances humaines à une équipe

### Software 

les dépendances logiciels. De la configuration des OS, à l'interface d'une bibliothèque, ou bien de l'API du cloud provider


### Hardware

les dépendances à tous type de hardware qui composent l'infra

### Réseaux

les dépendances aux réseaux

### Cloud

ci-présent, les dépendances vers les services du cloud provider


## Niveau de dépendances

## A

critique

## B

forte

## C

faible

# X (vide)

négligeable ou inexistante


