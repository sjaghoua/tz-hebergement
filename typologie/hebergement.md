# Typologie des hébergement

On va  ici définir une typologie des possibilités pour héberger une  infrastructure soutenant le déploiement d'une ou plusieurs applications/services. On va s'attarder en particulier à préciser et différencier les possibilités d'hébergement in-house, et les hébergements cloud. 

On ne parlera pas des possibilité d'exposer du contenue sans avoir à gérer une infrascture, par exemple via [wordpress.com](https://scenari.org/co/myScenari.html) ou [MyScenari](https://scenari.org/co/myScenari.html), qui restent tout de même des solutions fiable, performante et simple d'accès.

L'objectif de cette typologie est de pouvoir définir les différentes dépendances des infras selon plusieurs axes. 

Les types d'infrastructures présentés ne dépendent pas de l'échelle des infrastructures, même si l'étude se concentre principalement sur les dépendances des petites à moyenne structures. 

## 1 - hébergement In House (IH/a ; IH/p)


### Définition

On appelle hébergement in house, les infrastructures entièrement gérées et maintenues par la société ou association voulant exposer des services.

Une infrastructure in-house est composée d'ordinateurs reliés aux réseaux sur lesquels on veut exposer les services, le tout hébergé dans un local qui appartient à l'entité possédant l'infra. 


### Exemple d'hébergement in house.

* (amateur) un cluster de raspberry hébergé dans un appartement
* (pro) le datacenter d'une petite entreprise


## 2 - Bare Metals Owners (BMO/a ; BMO/p)

### Définition:

Le terme bare metals, ou serveur dédié;  fait référence aux types d’hébergement ou l’on administre une partie d'un datacenter. Contrairement à l'hébergement in house, on ne gère pas le datacenter en lui-même

Dans le cas Bare Metals Owners on est propriétaire des machines hébergées dans le datacenter géré par un tierce. 

On a accès à distance aux machines. On peut indirectement intervenir sur les machines, par le biais des propriétaires du datacenter qui peuvent s'occuper d'opération de maintenance.

### Exemple:

* Infra de picasoft hébergé dans un datacenter associatif


## 3 - Bare Metals Users (BMU/a ; BMO/p)

### Définition:


On loue des machines physiques dans un datacenter à une entreprise tierce qui est responsable de l'état des machines. On administre nous mêmes toute la partie software des machines. 


### Exemple:

* Un des chatons ? (à préciser)


## 4 - Cloud - IaaS (Infrastructure as a Service ) (IaaS/a ; IaaS/p)


### Définition:

On loue des machines virtuelles ainsi que des espaces de stockage. Le fournisseur cloud s'occupe du datacenter et de la virtualisation. 

On a accès à une interface propre au cloud provider qui permet de d'instancier et gérer une pool de machines virtuelles, sur lesquels on a les accès direct. 


### Exemple:

* L'asso Scenari

## 5 - PaaS (Platform as a Service) (PaaS/a ; PaaS/p)

### Définition 
Ici on loue l'accès à des services facilitant le déploiement et la gestion des applications. Ce type d'infra est souvent mixé avec les offres IaaS. 

On n'accède par le biais d'une interface propre aux cloud provider, à du software. 


On peut voir ces offres que des option "Black box" d'un software hébergé dans un datacenter du cloud provider.

à distinguer deux cas possibles pour le software qui seront important pour les dépendances :


* outil propriétaire au cloud provider
* outils open source (ou avec licence payante) qui pourraient tout aussi bien être déployés dans les autres cas d'infrastructures.


### Exemple

* Une base de données gérée. On a les droits d'accès sur la base de donnée, mais on ne s'occupe pas de la maintenir (et pas des des backups notamment). 
  * Pgsql managé (Open source)
  * MySql Enterprise (licence payante)
  * Big Querry ( Uniquement disponible chez Google Cloud)


* Kubernetes managé (open source) ou OpenShift (propriétaire)

* Serverless:
  * lambda (AWS)
  * Cloud Function (GCP)
  * openfaas (open source)


## Conclusion
![image](images/diagram-typo-hebergement.png "diagramme de la typologie d'hébergement")  


