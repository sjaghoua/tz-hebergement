# Conclusion

L’objectif de cette étude a été de proposer une méthode pour décrire les dépendances qui émerge lorsqu'on souhaite héberger un service sur une infrastructure informatique. On a donc défini une typologie des infrastructures informatiques, en partant du full in-house jusqu’au au cas complètement hébergé sur le cloud (PaaS). Notre méthode consiste donc à croiser la typologie des infrastructures avec la typologie des dépendances. 
Pour valider le fonctionnement de cette méthode, on l’a appliqué aux axes des sauvegardes. Même si la méthode a été construite en traitant les dépendances pour les backups, elle a été pensée pour se généraliser à d’autres axes, ce que l’on a fait avec l'axe de la disponibilité. 


Nous avons appliqué notre méthode pour décrire les axes suivant : 
* Backups 
* Disponibilité (IH_a et PaaS_p)

Une suite de notre travail (TODO) serait d'appliquer notre méthodologie aux axes suivant : 

* Disponibilité (à terminer) 
* Sécurité

* Réduction des coûts : ex une entreprise suite diminuer ses coûts par deux sur l'infra, elle peut utiliser notre méthode pour identifier ses dépendances d'un point de vue financier. 


* Scalabilité : ex : une association étudiante voit une forte augmentation de son trafic à cause d'une pandémie mondiale, elle peut utiliser notre méthode pour identifier ses dépendances d'un point si elle voulait multiplier par deux ses capacités. 


