# TZ TSH Héberger, s’héberger, se faire héberger : externalisations et dépendances

TZ TSH Héberger, s’héberger, se faire héberger : externalisations et dépendances 

Aujourd'hui de nombreuses entreprises et associations proposent des services pour répondre à des besoins sociétaux ou commerciaux par la création et le l’hébergement d'applications informatiques. C’est donc un enjeu d’être capable d'héberger des services sur internet, et on va s'intéresser dans notre étude aux infrastructures informatiques qui permettent cet hébergement. Plusieurs acteurs entrent en jeu dans la composition de ces infrastructures, notamment avec l’existence d'offres cloud ,de géant du numérique comme Google ou Amazon ou des acteurs plus locaux comme OVH, qui propose d’assurer une partie de l’hébergement de l’infrastructure. On héberge des services sur une infrastructure qui peut externaliser une partie de ses outils en se faisant héberger en partie auprès d’entités tierces. Une infrastructure informatique qui fait interagir plusieurs entités et par définition va créer des dépendances pour l’entreprise/association voulant héberger des services. On va chercher dans notre étude à décrire et expliciter ces dépendances.

Comment décrire et quelles sont les dépendances d’une infrastructure informatique qui héberge des services sur internet ?.


On a défini dans un premiers temps 5 axes de dépendances que nous voulons étudier et qui représente selon nous le spectre des de ces dépendances.

Chaque infrastructure est différente et ne présente pas donc les mêmes dépendances aux mêmes entités. On s'attardera donc à définir une typologie des infrastructures pensées pour répondre à ces questions de dépendances. On introduira aussi les notions de probabilité d'occurrence d’un événement et de son impact. Une infrastructure peut très bien avoir une forte dépendance envers une entité sans pour autant que l’entreprise/association qui la gère ait besoin de s’en soucier. Par exemple, si la probabilité d'occurrence d’un événement liée à ces dépendances est négligeable ou bien son impact faible. Ces notions sont nécessaires car deux infrastructures qui peuvent paraître similaires techniquement n’ont pas forcément les mêmes approches ni les mêmes objectifs, c’est le cas notamment lorsqu' une infrastructure est amateur ou bien professionnelle.



