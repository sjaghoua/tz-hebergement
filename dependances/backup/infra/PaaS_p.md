# Scénario

L'infra est hébergé sur un cloud international, les services sont exposés sur un service de kubernetes managé, le cluster est composé d’une dizaine de VMs, on a pas accès directement aux machines, mais on peut y déployer n'importe quel service par l’interface de kubernetes et du cloud provider. Les données des applications sont hébergées sur des volumes qui sont des abstractions pour des espaces de stockage, on accède donc jamais ni physiquement ni virtuellement au système de fichier; même si on peut accéder aux données par le biais des outils de kubernetes et du cloud provider.


La perte de données est critique puisque ce sont des données clients.


## R


### Cloud : A/X/C 

Il n'y a pas de redondance directe, mais cela est géré par le cloud provider. 


## Backup I (Dépendances/Risques/Impact)

Ces backups correspondent aux backups des services que le cloud provider fournit, base de données managée, configuration du cluster Kubernetes ... Elles se font de façon automatique par le cloud provider, on peut si l’on veut restaurer ces backups à tous moments


### Human : X/X/X

Il est très simple de remonter les backups, les équipes des clouds provider peuvent même nous y aider si besoin.

### Hardware : X/X/X

On ne considère pas directement le hardware du cloud provider à cette échelle d'abstractions.


### Software : A/C/A

Dans ce type d’infra, tout ce que l’on manipule  est du software et des interfaces, c'est donc une forte dépendance. On parle même de Infrastructure as Code lorsque on utilise des outils comme terraform ou ansible pour gérer des tels infrastructures.

Les risques de problèmes avec ces interfaces est faible, car elles sont utilisées par d'autres entreprises et sont donc très stables. L'impact d'un problème software à ce niveau est critique car cela entraîne l'impossibilité de remonter les backups puisque que ce sont les seuls accès. 


### Network : X/X/X

Tout se fait automatiquement puisque l'on accède que à l'interface pour restaurer les backups.

### Cloud: A/C/A

C'est le cloud provider qui s'occupe de l'orchestration et la maintenance de ces backups, la dépendances est donc ultime, les risques faible mais l'impact est critique, si le cloud provider perd des données on ne peut rien y faire. 


## Backup D (Dépendances/Risques/Impact)

Les Backups sont automatisées par des crons jobs sur kubernetes, elles se font de façon quotidienne. Par des outils développés dans l’entreprise. Les données sont compressées et chiffrées lors de ce processus et sont stockées sur des buckets de type glacier du cloud provider. Ces buckets permettent de stocker des fichiers à moindre coûts, ils fonctionnent comme un “drive” il faut passer par l’interface cloud pour manipuler ces buckets.


### Human : C/X/A

Les savoirs sont partagés, tout est documenté. La dépendance humaine est faible car répartie. L'impact du critère humain est important car la restauration n'est pas automatique. 

### Hardware : X/X/X

On ne considère pas directement le hardware du cloud provider à cette échelle d'abstractions.


### Software : A/B/B

Là aussi tout ce qu’on manipule est software et interface, c'est donc une fortes dépendances, les risques de problèmes avec ces interfaces est faible, car elles sont utilisées par d'autre entreprise et sont donc très stables. L'impact d'un problème software à ce niveau est fort car cela empêche la restauration immédiate des backups, mais les fichiers de sauvegarde seront toujours disponibles ( sauf en cas de corruption ou de perte de données mais il faut se référer à la dépendance Cloud). 


### Network : A/B/B

Les sauvegardes se font par la communication entre des machines virtuelles et des services de stockage (buckets et glacier) du cloud provider. La dépendance aux réseaux est donc très forte à la fois pour sauvegarder et restaurer. On a pas d'accès physique aux données. Les risques sont faibles mais l'impact est important. 

### Cloud: B/C/A

C'est le cloud provider qui gère le stockage des fichiers dans les buckets. C'est donc une dépendance importante. Les risques de problèmes sont faibles, mais l'impact est immense puisque cela reviendrait à une perte de données.



