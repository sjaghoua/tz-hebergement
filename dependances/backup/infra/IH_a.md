# Scénario


L'infra est hébergée dans un appartement sur un réseau informatique et électrique personnel derrière une box d'un grand opérateur. Elle est composée de deux RaspberryPI (R1 et R2) chacune disposant de deux disques durs (sans aucun usage de RAID). L'administrateur de l'infra vit dans l’appartement. Les services sont hébergés pour l'utilisation individuelle de l'administrateur et d'une vingtaine d'amis. L'une des deux Raspberry (R1) est accessible sur le réseau Internet (l'administrateur dispose d'une IP fixe et a redirigé les ports sur sa box), l'autre (R2) n'est accessible que sur le réseau local. Les services proposés sont par exemple:

  * un espace d'échange de fichiers (SFTP) 
  * un service de discussion (Matrix)
  * un service de gestion d'événements (Mobilizon)
  * un drive (NextCloud)
  * un serveur jeu (ex: minecraft)
  * un VPN
  * ...


Les utilisateurs sont avertis que leurs données peuvent être compromises (vol de l'infra ou du disque de backup distant).

L'arrêt des services n'est pas critique pour les utilisateurs.



## R


Il n'y a pas de redondances des données dans l’infra


## Backup I (Dépendances/Risques/Impact)

Le *backup interne* est automatisé par un script shell avec une tâche cron qui tourne sur les machines de l’infra. Le script utilise la commande rsync. Il s'exécute chaque nuit à 3h AM et dure en moyenne 30 minutes. Seules les données de R1 sont sauvegardées. Le volume moyen est de 1 Le différentiel moyen sur 24h est de 10 Go. Elles sont sauvegardées sur un disque de R2 totalement dédié au backup. 

Les backups ne sont pas chiffrées.


### Human : A/B/B

Même si les backups internes se font de façon automatique. Il y a une forte dépendance à celui qui détient le savoir sur cette orchestration. Une sauvegarde que l’on ne sait pas restaurer est aussi utile que de ne pas avoir de sauvegarde.

L'administrateur doit vérifier que les backups sont bien réalisés. Faire des tests de restauration.

### Hardware : B/A/C

Le backup local étant réalisé toutes les nuits, si R1 cassé alors on ne perd que les données de la journée en cours.

Le backup distant n'est réalisé que tous les mois environ, donc si R1 et R2 sont détruites (incendie, inondation...) ou volées, on perd potentiellement plusieurs semaines de données.

Les disques durs de sauvegarde sont centralisés ( et c’est encore pire si il n’y en a qu’un seul). Une panne hardware (i.e une défaillance des disques dur)  sur l’un des nœuds de backup entraîne de la perte de données sauvegardé pour plusieurs services (voir pour tous les services), mais empêche aussi . 
Il est peut-être plus rare que trois disques tombent en panne la même journée, mais même dans le cas où il y a plusieurs nœuds de stockage, le fait que les disques durs soient centralisés entraîne une forte dépendance aux niveaux des backups. Par exemple en cas d'incident ou de dégâts des eaux et c’est justement dans ces cas qu’on l’on aimerait avoir des backups fonctionnelles pour tous remonter.


### Software : C/B/C

Les disques durs de sauvegarde sont reliés des machines ou sont exécutés du code qui vont gérer les sauvegardes. 
Dans le cas des sauvegardes avec rsync, ce code est “simplement” l’os est le système de fichiers des machines. 
Il reste une dépendance puisque ce logiciel est une interface essentielle pour accéder aux sauvegardes mais aussi pour les orchestrer. 
Dans le pire des cas, un OS mal configuré ou contenant des failles ou des bugs pourrait corrompre les sauvegardes. 
Cette dépendance est légère puisque conceptuellement le hardware est séparé du software qui l’utilise, est qu’un software défectueux n'entraîne pas une perte de données mais une latence dans l’accès (il faut changer/réparer le software) sauf bien sûr en cas de corruption. 


### Network : A/C/X

Ce type de backup sont des transferts de données entre deux disques durs connectés à des machines différentes reliées physiquement aux mêmes réseaux locaux. 

Une panne de réseaux est critique pour les backups mais est peu probable et n’a pas d' impact car rien n’est perdus et il est facile de remettre en place le processus de sauvegarde


### Cloud: X/X/X

## Backup DP (Dépendances/Risques/Impact)


Le *backup distant* est réalisé manuellement tous les mois par l'administrateur par une copie rsync réalisée sur un disque externe USB3. Le différentiel moyen est de 100 Go, la durée moyenne est de 3h. Le disque est déposé chez le voisin de l'administrateur. 



### Human : A/A/C

Le backup est réalisé manuellement et est déplacé chez un voisin.


Il y a une double dépendance humaine, un risque fort de problème (un voisin absent, une dégradation ...) l' impact est faible pour les backups car les backups journalière existent.


### Hardware : A/X/C

Un disque qui sauvegarde les données est l'unique point de ces backups. La dépendance aux hardware est donc absolue. 
Les risques sont négligeable, le disque n'était utilisé qu'une fois par mois
Les impact sont faibles

### Software : X/X/X


### Network : X/X/X

Tout se fait en physique
### Cloud : X/X/X
