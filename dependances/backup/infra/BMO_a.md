# Scénario


L'infra est hébergée dans un datacenter associatif. Elle est composée de deux machines type rack serveur.  L'accès aux machines se fait à distance. L'association est propriétaire des machines mais n'y a pas l'accès direct. Elle peut remplacer ou récupérer un disque endommagé via l'équipe du datacenter.

Les services hébergé sont du type :

  * Chat (Mattermost)
  * Éditeur de note collaborative (Etherpad)
  * VoIP (Mumble)
  ... [voir](https://picasoft.net/)


Les utilisateurs sont avertis que leurs données peuvent être compromises, dégradation ou vol de données. Les services hébergés sont à destination de beaucoup d'utilisateurs proches du cadre associatif. 


La perte de données est donc grave, même si l'infra a un caractère amateur. 



## R

Toutes les bases de données sont stockées sur des disques qui utilisent la technologie RAID. Elle permet de stocker des données sur plusieurs disques en même temps et d'assurer une redondance de celle- ci tout en optimisant le coût de stockage et le temps d'accès aux données. 
Cela permet donc d'éviter la perte systématique en cas de panne. 



### Human : C/X/X

La technologie RAID une fois bien configurée fonctionne automatiquement sans intervention humaine.


### Hardware : B/C/B

La technologie RAID permet d'éviter la perte de données en cas de problème hardware. Elle repose principalement sur des algorithmes de répartition de données mais aussi des technologies de stockage. 

Elle est donc fortement dépendante du hardware mais pas de façon critique par la redondance induite. 
Les risques de perte de données avec la technologie RAID est que plusieurs disques tombent en même temps, elle est donc faible, mais pas négligeable car les disques sont stockés au même endroit (surchauffe local, dégâts physiques ...). 
Les impacts sont faibles car même s' il y a perte de données mais pas critique à cause du caractère associatif de l'infra. 


### Software : A/X/B

La technologie RAID repose sur des algorithmes pour assurer une redondance optimisée des données. Il y a donc une forte dépendance aux softwares. Le risque qu'un problème arrive à cause du software est négligeable, le soft étant déjà éprouvé. (sauf en cas de mauvaise configuration mais cela est une dépendance humaine).

Les impacts sont mixtes car la donnée est dans tous les cas accessible par un accès  physique sur les disques même en cas de problème software. Cependant dans ce cas les accès physique sont limité 


### Network : X/X/X


La technologie RAID n'utilise pas directement le réseau externe.




## Backup I (Dépendances/Risques/Impact)

Il y a deux types de backups interne :

* Backups journaliers de l'ensemble des machines virtuelles (une grosse image disque), pour récupération en cas de grosse urgence. Il y a deux machines physiques sur lesquelles sont distribuées les machines virtuelles, et les backups sont échangés entre les machines physiques pour pallier une défaillance
* Backups plusieurs fois par jour des bases de données des services choisis. Les backups sont gardés avec une rotation configurée pour 1 backup par an, puis par mois, semaine, jour, heure, etc.  les scripts d'export des BDD sont des scripts custom.


### Human : A/B/B

Même si les backups internes se font de façon automatique. Il y a une forte dépendance à celui qui détient le savoir sur cette orchestration. Une sauvegarde que l’on ne sait pas restaurer est aussi utile que de ne pas avoir de sauvegarde.

Tout est documenté sur le wiki mais les membres de l'association ne sont pas dévoués à temps plein à l'infra, étant des étudiants ou des professeurs. La dépendance humaine est donc critique, les risques qu' aucun des membre ne soit disponible en cas de problème probable. Et l'impact est important car elle délaisse la restauration de backups en cas de problème.

### Hardware : A/C/B


Il y a une dépendance moyenne aux hardwares grâce à la redondance (voir Backup R) et ceux même si tout est stocké sur les disques. 

Grâce à la redondance, les risques de pertes des données à cause de panne hardware isolée sont très faibles. 

Tous les backups internes sont centralisés dans le datacenter associatif. Une panne plus globale peut donc corrompre les données de backups. Un incendie ou un dégât des eaux par exemple. Le but des backups étant justement de pouvoir récupérer des données en cas de problème, si les backups se corrompre en même temps que les services tombent à cause d’un incident, tout est perdu et c’est comme si il n’y avait pas eu de backup.

Le risque d’un tel incident est faible mais les impacts sont énormes. Pour un incident local l'impact est moyen voir faible grâce à la redondance




### Software : C/B/C


Les disques durs de sauvegarde sont reliés des machines ou sont exécutés du code qui vont gérer les sauvegardes.
 Dans ce cas il y a deux types de dépendances software, l’os est le système de fichiers des machines. Il reste une dépendance puisque ce logiciel est une interface essentielle pour accéder aux sauvegardes mais aussi pour les orchestrer.  Dans le pire des cas, un OS mal configuré ou contenant des failles ou des bugs pourrait corrompre les sauvegardes. Mais aussi et surtout le code des scripts de backups. Mais celui-ci est exclusivement maintenu par les membres de l'association, c’est donc plutôt une dépendance humaine interne qu’une dépendance softwares.

Il subsistent donc principalement une dépendance aux interfaces d’accès aux machines.
Cette moyenne puisque même si conceptuellement le hardware est séparé du software qui l’utilise, est qu’un software défectueux n'entraîne peut être pas une perte de données mais rompt l’accès aux sauvegardes. Et dans notre cas les disques ne sont pas directement (ou en tout cas pas facilement) accessibles. Il faut en effet passer par les membres qui hébergent le datacenter. Il n’y aura donc pas de pertes de données, mais potentiellement une grande latence dans la restauration des backups en cas de problème software.

Les risques sont moyens, un problème au niveau de l’OS après une mise à jour n’est jamais à exclure ou bien une mauvaise manipulation qui toucherait à des fichiers systèmes sensibles. Les conséquences sont fortes car on a pas l’accès direct aux machines.


### Network : A/B/A

Il faut passer par le réseau pour orchestrer les backups. C’est la première interface pour communiquer avec les serveurs et donc gérer les backups. La dépendance est donc forte pour les backups internes.

Les risques sont moyens, une panne de réseau peut arriver. Les conséquences sont fortes, puisque on ne peut pas remonter les backups sans accéder aux machines et l’accès physique est contraint.


### Cloud: X/X/X


L’infra est dépendante du datacenter associatif dans le cadre des backups lorsqu' on veut accéder aux machines pour récupérer des disques endommagés ou inaccessibles.

C’est une forte dépendance, les risques d’avoir besoin d’accéder aux machines sont faible, car cet option est utilisée lorsque toutes les autres forment de récupération de backups ne fonctionnent pas. Les impacts de ne pas pouvoir accéder aux machines sont critiques car on perd les données si toutes les autres méthodes d’accès à distance ne fonctionnent pas.


## Backup DP (Dépendances/Risques/Impact)


Il n'y a pas de backup déporté pour cette infra


### Human : A/A/C
### Hardware : A/X/C
### Software : X/X/X


### Network : X/X/X
### Cloud : X/X/X
