# Scénario

C’est le cas d'une entreprise qui possède son propre datacenter. Qui hébergent plusieurs gros services pour ses clients mais aussi des services en interne. Qui ne peut pas se permettre de perdre des données et a besoin de les chiffrer. Il y un tout un processus industriel (à l’inverse du cas amateur) pour orchestrer les sauvegardes mais aussi pour monitorer son bon fonctionnement. C’est une équipe à part entière qui s'occupe de ces backups.



## R

Toutes les bases de données sont stockées sur des disques qui utilisent la technologie RAID. Elle permet de stocker des données sur plusieurs disques en même temps et d'assurer une redondance de celle- ci tout en optimisant le coût de stockage et le temps d'accès aux données. 
Cela permet donc d'éviter la perte systématique en cas de panne. 



### Human : C/X/X

La technologie RAID une fois bien configurée fonctionne automatiquement sans intervention humaine.


### Hardware : B/C/A

La technologie RAID permet d'éviter la perte de données en cas de problème hardware. Elle repose principalement sur des algorithmes de répartition de données mais aussi des technologies de stockage. 

Elle est donc fortement dépendante du hardware mais pas de façon critique par la redondance induite. 

Les risques de perte de données avec la technologie RAID est que plusieurs disques tombent en même temps, elle est donc faible, mais pas négligeable car les disques sont stockés au même endroit (surchauffe local, dégâts physiques ...). 
Les impacts sont  importants car il y a perte de données mais pas critique puisque d'autres types de backup sont présents. 


### Software : A/X/B

La technologie RAID repose sur des algorithmes pour assurer une redondance optimisée des données. Il y a donc une forte dépendance aux softwares. Le risque qu'un problème arrive à cause du software est négligeable, le soft étant déjà éprouvé. (sauf en cas de mauvaise configuration mais cela est une dépendance humaine).

Les impacts sont mixtes car la donnée est dans tous les cas présentée en physique sur les disques même en cas de problème software. Cependant dans ce cas les accès physique sont limité 


### Network : X/X/X


La technologie RAID n'utilise pas directement le réseau externe.



## Backup I (Dépendances/Risques/Impact)


Les backups sont orchestrés par des outils spécialisés qui se chargent aussi du chiffrage. 


### Human : C/X/A

La dépendance à l’équipe (dans sa globalité) qui est responsable des backups est forte, mais est faible individuellement, puisque justement dans ce cas les savoirs sont partagés dans l'entreprise.



### Hardware : A/C/B

C’est le nerf de la guerre pour les backups ( qui sont rappelons le essentiel au bon fonctionnement de l’entreprise). Chaque panne hardware peut entraîner de la perte de données. La dépendance est donc très forte.

Même si ces machines de backups sont centralisées, une défaillance globale du hardware qui soutient cette orchestration de backups est dramatique mais très rare à moins d’un incident global (comme un feu chez OVH). 

Cependant grâce aux technologies de redondance et aux backups déportés, ces backups sont dupliqué. Le périmètre de l’impact peut donc être contenu.  


### Software : B/B/A


Dans ce cas, les backups sont orchestrés par des outils spécialisés qui se chargent aussi du chiffrage ( et déchiffrage )  des backups. C’est donc aussi un point central pour les backups. C’est une forte dépendance. On devient dépendant des choix des développeurs

  * sur l’interface (API) pour utiliser le software, si les devs décident de rendre obsolète certaine façon d’utiliser l’outil de backup , cela implique une refonte des processus qui se base dessus.


  * Sur la sécurité, les choix techniques de chiffrages, la réactivité en cas de faille de sécurité etc.
Dans le cas de soft propriétaire on devient encore plus dépendant aux développeurs de ces outils puisque l’on a pas la main sur les sources du projets. Il y a donc une dépendance vis à vis de l'éditeur logiciel très forte pour les backups (notamment financière mais on en parlera dans la partie appropriée).


### Network : C/C/X

L’accès par le réseau aux machines est une dépendance faible pour les backups. 

En cas d’incident ou on aurait besoin de restaurer certaines backups, si l'accès réseau n’est pas disponible, on peut passer par l’accès physique aux machines.

De plus, un problème de réseaux n'entraîne pas une perte de données. 



### Cloud: X/X/X

## Backup DP (Dépendances/Risques/Impact) 

Les backups chiffrées sont envoyé tous les semaines dans un serveur de données 
spécialisé. Ce serveur de données est aussi in-house, maintenue de la même façon que le datacenter principal. La seule différence est sa taille qui est plus petite, et son usage qui est orienté que pour les backups. On retrouve donc exactement les dépendances, risques et impact que pour les Backup I (Interne)


