# Scénario

C’est le cas d'une entreprise qui possède ses propres machines qui sont hébergées dans un datacenter. Qui hébergent plusieurs gros services pour ses clients mais aussi des services en interne et qui ne peut pas se permettre de perdre des données et a besoin de les chiffrer. Il y un tout un processus industriel (à l’inverse du cas amateur) pour orchestrer les sauvegardes mais aussi pour monitorer son bon fonctionnement. C’est une équipe à part entière qui s'occupe de ces backups.

Ce type d'hébergement est proche de celui IH_p en tout cas en considérant l'axe des backups. La seule différence se trouve au niveau des accès machine qui est plus restreint car il faut passer par ceux qui gère le datacenter pour n’importe quelle opération.

Puisque les deux cas sont proches, toutes les sections qui ne sont pas reprises ici sont à hérité du cas IH_p.


## R

Hérité de IH_p

## Backup I (Dépendances/Risques/Impact)





### Network : B/C/B

Puisque l'on a plus d'accès rapide aux machines, un problème réseau peut empêcher la restauration des sauvegardes, la dépendance est donc forte.

Les risques restent faibles mais pas négligeables.

Les impacts sont importants car on ne peut plus restaurer de backup et pour l’entreprise cela est fort mais pas critique car il n’y a pas de perte.


### Cloud: X/X/X

## Backup DP (Dépendances/Risques/Impact) 

Les backups chiffrées sont envoyé tous les semaines dans un serveur de données 
spécialisé. Ce serveur de données est aussi dans un datacenter ou l'on est propriétaire des machines, maintenue de la même façon que le datacenter principal. La seule différence est sa taille qui est plus petite, et son usage qui est orienté que pour les backups. On retrouve donc exactement les dépendances, risques et impact que pour les Backup I (Interne)


