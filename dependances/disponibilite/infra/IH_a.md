# Scénario


L'infra est hébergée dans un appartement sur un réseau informatique et électrique personnel derrière une box d'un grand opérateur. Elle est composée de deux RaspberryPI (R1 et R2) chacune disposant de deux disques durs (sans aucun usage de RAID). L'administrateur de l'infra vit dans l’appartement. Les services sont hébergés pour l'utilisation individuelle de l'administrateur et d'une vingtaine d'amis. L'une des deux Raspberry (R1) est accessible sur le réseau Internet (l'administrateur dispose d'une IP fixe et a redirigé les ports sur sa box), l'autre (R2) n'est accessible que sur le réseau local. Les services proposés sont par exemple:

  * un espace d'échange de fichiers (SFTP) 
  * un service de discussion (Matrix)
  * un VPN
  * Des sites web statiques

R1 héberge le SFTP et le VPN , R2 héberge matrix et les sites web.

L'opérateur est Orange, c'est une livebox qui fait office de routeur. La redirection vers les raspberry pi des requêtes réseaux se fait par l’interface d’administration du NAT du routeur.

Il n’y a pas de reverse proxy pour gérer automatiquement la redirection des ports.

Le SFTP est hébergé sur le port 22, le VPN sur le port 1194, le matrix sur le port 5000 et le page web sont hébergés sur une plage de port entre 8000 et 9000.

Un serveur serveur ssh et open vpn sont installés sur l’os des raspberry pi (pas de virtualisation ou de containerisation), ainsi qu’un serveur apache HTTP pour gérer les pages statiques.

Les utilisateurs sont au courant que les services ne sont pas conçus pour être hautement disponibles




## Disponibilité (Dépendances/Risques/Impact)

### Human  A/A/B

Le facteur humain apparaît dans la problématique de disponibilité lorsque un des services tombent et qu’il faut opérer pour le relancer.

Dans ce cas, il n’y a qu'une seule personne qui s’occupe de l’infra et qui peut donc s’en charger. Il y a donc une dépendance très forte vers le  caractère humain. 

Il est probable qu’une infra in house amateur de ce type ait épisodiquement besoin d’une action humaine pour redémarrer des services. Cela peut être une coupure de courant, d’une machine qui crache, le processus  du serveur apache qui crashe à cause d’une trop forte demande… . 

L’impact est modéré car les utilisateurs n'attendent pas 99% de disponibilité.




### Hardware A/B/B 

La dépendance hardware est forte du point de vue de la disponibilité. En effet tout problème  de hardware, surchauffe, crashe, performance, disque dur endommagé peuvent provoquer une indisponibilité. Même s' il y a deux machines, il suffit qu’une seule tombe pour entraîner de l’indisponibilité, il n’y a pas de résilience ou de réplication des services.

Le risque de problème hardware est moyen. Les machines ne sont pas du matériel professionnel mais leur utilisation est modérée et elles ne sont pas utilisées à 100% de leur capacité en permanence.

Les impacts sont modérés pour les mêmes raisons que le critère humain, les utilisateurs  sont avertis.


### Software  A/A/C

On parle ici uniquement du software qui entoure le déploiement du code des applications. Par exemple pour le matrix, on ne considère pas les dépendances vis-à-vis des développeurs de ce projet, ni de la possible présence de bug. On considérera par contre le serveur apache puisque c’est une software intermédiaire dans le déploiement des pages html.

Les dépendances software sont très fortes dans ce cas. En effet puisque rien n’est virtualisé ou conteneurisé, il suffit de casser une dépendance des applications pour que celle-ci ne puisse plus s'exécuter. Cela peut être par exemple l’interpréteur python mal configuré après une mise à jour qui empêche d'exécuter tous les codes tournant avec python.

Les risques de problèmes softwares sont forts. Pour être sécurisé il faut très régulièrement mettre le système à jour pour éviter de corriger les failles récentes, puisque l’infra à des ports ouverts sur tout l’internet. Il y a donc une importante dynamique de changement de software au sain de l’infra et des OSs. Raspbian (OS debian sur raspberry pi) n’est pas un logiciel pour la mise en production d’applications sur internet, (contrairement à un RedHat OS par exemple). De plus, l'architecture ARM est moins éprouvée en production que le plus classique x86 est il est plus probable de rencontrer des problèmes d’incompatibilité après une mise à jour.

L’impact est faible, les problèmes software ne sont pas forcément localisés, le serveur apache peut tomber sans que le VPN ne soit affecté.




### Network : A/C/X

La dépendance aux réseaux est extrêmement forte. C’est un point névralgique de l’infrastructure. Il y a en particulier une très forte dépendance envers le fournisseur réseaux. On en parle plus en détail dans la partie réseaux.md TODO link.

Mais pour résumer, il n’y à pas dans tous les cas une assurance d’avoir une IP fixe et le routeur peut dans certain cas limiter certaines fonctionnalités de l’auto-hébergement, voire de le rendre impossible.

Les risques existent est dépend de l’opérateur et de la localisation de l’infra.

L’impact peut être très important, jusqu'à empêcher l’utilisation d’une offre internet “classique” d’un opérateur national.


### Cloud: X/X/X


