# Scénario

L'infra est hébergé sur la GCP (Google Cloud Platform), les services sont exposés sur un service de kubernetes managé (GKE), le cluster est composé d’une dizaine de VMs, il n’y a pas accès directement aux machines, mais on peut y déployer n'importe quel service par l’interface de kubernetes et du cloud provider.

Toutes les applications sont conteneurisées et sont gérées dynamiquement par kubernetes. Les applications sont dispatché en micro-service et s’imbriquent avec des outils propre à la GCP comme BigQuery ou les clouds functions.

Plusieurs services partagent le même point d’entrée dans l’infra, il y a donc une reverse proxy  (nginx) à l’entrée de l’infra qui redirige les requêtes vers les bon services.

Il a deux types de services hébergée sur l’infra: les services internes sur un réseau privé et les services client (externe) exposés sur internet.

Les savoirs doivent être disponibles en permanence, les services externes sont utilisés par les clients en permanence et une indisponibilité pour des services internes bloque le fonctionnement interne de l’entreprise.


## Disponibilité (Dépendances/Risques/Impact)

### Human  A/X/B

Dans ce cas, une équipe de plusieurs ingénieurs se charge de d’intervenir dans l’urgence. On pourrait penser que le facteur humain n’est donc pas si important, c’est en tout cas le raisonnement que lors du travail sur les backups, en effet le savoir est partagé il n’y a pas une forte dépendance envers un être humain. Cependant dans le cas de la disponibilité, en tout cas dans une infra professionnel, c’est chaque minute d’indisponibilité qui est dommageable. Si cela arrive en pleine nuit, toute l’équipe n’est pas censée travailler, il y a donc une dépendance forte envers les ingénieurs d'astreintes.

Lorsque les astreintes sont respectées, il est très peu probable qu'un problème d'indisponibilité persiste à cause du facteur humain, mais les impacts seraient énormes car cela immobiliserait l'infra.


### Hardware X/X/A 

La dépendance aux hardwares est faible. En effet pour l’expliquer il faut rentrer un peu plus en détail dans le fonctionnement de kubernetes. Il permet d’avoir plusieurs réplicas d’une même application sur plusieurs machines différentes et en cas de chute d’un des replicas il est capable de le remonter tout seul sur une des machines disponibles. Les applications conteneurisées fonctionne seule puisque elles sont empaquetées avec leurs dépendances et fonctionnent donc de la même façon qu’importe la machine hôte. Kubernetes orchestre donc les conteneurs de façon dynamique en les allouant à une machine est le déplaçant si nécessaire. En cas de panne hardware, une machine qui tombe par exemple, kubernetes est capable de garantir une disponibilité quasi constante.

Les risques de problèmes hardwares sont faibles, tout se cache derrière les VMs de la GCP qui sont aussi redondantes.

Les impacts d'une panne hardware généralisée sont fort.


### Software  A/C/A


La dépendance aux software est forte puisque c’est Kubernetes qui orchestre tous les déploiements de pods.

Il y a aussi une forte dépendance au niveau du reverse proxy puisque c’est le point d’entrée de l’infra depuis l’extérieur et que c’est lui qui redirige les bonnes requêtes vers les bons services

Puisqu' il existe un point névralgique (le reverse proxy) la dépendance au software est critique. 

Les risques sont faibles lorsque tout est bien configuré, l’impact est ultime car tous les services peuvent tomber en même temps.



### Network (via Cloud) : A/C/A

Ici la partie network est entièrement gérée par le cloud provider, c’est donc une double dépendances dont on fera référence uniquement en parlant de réseau.

Il y a une forte dépendance au niveau du réseau externe, puisque la disponibilité n'est possible qu'en étant connecté aux réseaux externes. C’est le cloud provider qui s’occupe de nous fournir des IPs ainsi que de fournir une bande passante entrante et sortante.

Le cloud provider est aussi un intermédiaire entre les requêtes entrantes et nos services. Il met en place des loadbalancer commun à plusieurs infrastructures pour gérer les requêtes entrante. Il y a donc une très forte dépendance au cloud provider sur la question du réseau.

Les risques de problèmes sont faibles mais l’impact est ultime.


### Cloud: A/C/A

En plus de la question réseau, il y a une dépendance aux niveaux de la gestion de Kubernetes. En c’est le cloud provider qui manage le service d’orchestration des déploiements, cela revient en terme plus technique, à dire que le cloud provider manage le noeuds master de kubernetes, il s’occupe notamment de le mettre à jours et de gérer les droits. Puisque c’est cet outil qui gère toute le déploiements des services c’est une ultime dépendance envers le cloud provider.

Les risques de problème sont faibles mais l’impact est ultime.



