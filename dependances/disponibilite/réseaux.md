# Dépendance vis-à-vis du fournisseur réseaux

Dans cette partie on va traiter la question de dépendances de nos 10 types d’infra par rapport à la question de l’accès aux réseaux. On va notamment étudier l’accès à une adresse IPV4/IPV6 pour exposer un service.

Pour notre cas de dépendance, on va regrouper nos 10 catégories.

Les Bare Metals Users,Cloud IaaS, Paas, vont être traiter comme une seule et même catégorie. 


## 1 - hébergement In House (amateur)

Pour les axes de travail sur la question du réseaux (pour le in-house) je vais m’inspirer du travail suivant : (https://wiki.auto-hebergement.fr/fournisseurs/fai) qui est malheureusement vieux (2012/2013) et qui ne reflète plus l’état actuel.

### Possède-t-on une adresse IP static ?

ne pas posséder une IP fixe rend l’hébergement in house compliqué voir impossible sans perte de disponibilité. 

#### Chez orange:
c’est un option supplémentaire réservé aux proffesionnels, https://boutiquepro.orange.fr/pro/options-internet/ip-fixe

autrement l’ip est dynamique, elle peut être amené à changer.

A quel point ?

https://assistance.orange.fr/livebox-modem/toutes-les-livebox-et-modems/installer-et-utiliser/piloter-et-parametrer-votre-materiel/le-parametrage-avance-reseau-nat-pat-ip/gerer-votre-adresse-ip/adresses-ip-les-elements-a-connaitre-_238182-760947

Il y a deux types d’adresse IP non static chez orange :

les **IPs dynamiques** : elles sont modifiées périodiquement par Orange ou en cas de reconnexion de la Livebox

les **IPs préférentiels** : elles sont stables dans la durée et ne changent qu'en cas de déconnexion de la Livebox (de plus de 7 jours) : Mais *orange peut procéder au renouvellement de votre adresse IPv4 si cela est nécessaire*

En pratique, qu'est ce que cela veut dire ? Difficile de se documenter sur le sujet, Orange ne précise pas la période de changement.


Cela varie beaucoup. Mais dans tous les cas on n’a pas l’assurance d’avoir une IP fixe chez Orange sans payer l’option professionnelle.

#### Chez free

l’option d’ip statique est proposé à tous les utilisateurs free en *zone non dégroupée* https://free.fr/assistance/54.html

(les dégroupages : https://www.echosdunet.net/free/zone-degroupee)

Cependant, les IPV4s se faisant de plus en plus rare la tendance change, en tout cas selon cette article : https://bouchecousue.com/blog/free-ip-publique-v4-full-stack/
On parle d’ici d’adresse IPs partagée (NAT), avec une plage de port réservée par utilisateur.
Dans ce cas on ne peux même pas garantir d’avoir les port http et https de base, encore plus embêtant que les les IPs dynamiques.

(https://www.numerama.com/tech/145703-free-peut-attribuer-la-meme-adresse-ip-a-plusieurs-abonnes.html)

#### Chez SFR

En 2020 d’après la réponse d’un community manager de chez SFR, c’est comme chez orange, c’est une option pro. (https://la-communaute.sfr.fr/t5/installation-et-param%C3%A9trage/ip-fixe-avec-sfr-box-8-fibre/td-p/2211405) (https://www.sfr.fr/pro/contenus/services/)


### Conclusion:

En 2021, chaque utilisateur a une IP propre, chez certain opérateur, notamment orange, elle n’est pas obligatoirement fixe. Cependant, aucun abonnement non professionnel ne *garantie contractuellement* une adresse IP fixe. 
Il est donc tout à fait possible, de s’auto-héberger avec ces opérateurs nationaux, mais il faut être prêt à migrer ses DNS si un jour l’ip change.

Les offres professionnelles à l’inverse garantissent contractuellement  une IP fixe.





#### Pénurie d’IPV4

Il faut garder en tête qu’il y a une pénurie d’IPV4
(https://www.arcep.fr/la-regulation/grands-dossiers-internet-et-numerique/lipv6/suivi-epuisement-adresses-ipv4.html , https://www.arcep.fr/cartes-et-donnees/nos-publications-chiffrees/transition-ipv6/barometre-annuel-de-la-transition-vers-ipv6-en-france.html , https://www.nextinpact.com/article/29808/108413-face-a-penurie-dipv4-bien-triste-etat-deploiement-dipv6-en-france ) 
Et que tous les opérateurs ne sont pas près pour l’IPV6. (d’après l’arcep juste en haut).


On peut donc supposer, d’après cette courbe d’épuisement des IPV4s, que les prix des IPV4s vont augmenter, et donc que les pools d’ip disponibles par opérateurs vont diminuer.

Si l’on suppose que le nombre d’offre fibre/ADSL restent le même (*)  alors on peut supposer que les opérateurs utiliseront sûrement des techniques de regroupement, en divisant les ports par utilisateurs et non plus les IPs, comme ça est déjà arrivé. 

(https://www.numerama.com/tech/145703-free-peut-attribuer-la-meme-adresse-ip-a-plusieurs-abonnes.html)

() C’est une hypothèse qu’il faut questionner, il se peut que les offres 4G et 5G (qui elles sont IPV6 ready d’après l’arcep) vont rebattre les cartes, et que le nombres d’offres Fibre va diminuer.

![image](dependances/images/202006_arcep_rapport_etat_internet_2020_3_ipv6_6.png , "pénurie ipv4")

![image](dependances/images/202012_arcep_barometre_ipv6_fixe_2.png , "ipv6 ready")




### Le matériel fourni par les opérateurs nationaux est-il compatible avec l’hébergement in-house ?


Le type de BOX, qui va servir de modem à un impact sur ce qu’on l’on peut faire avec son infrastructure. On est dépendant de ce que l’opérateur nous impose comme matériel.

En 2021 (et c’était déjà le cas en 2012 d’après …) les quatres opérateurs nationaux fournissent les indispensables pour faire tourner une infrastructure, à savoir :accéder aux paramétrage du modem et être capable de rediriger les ports.

Cependant certains problèmes persistent avec ces boxs qui ne sont conçus pour faire de l'auto-hébergement.

sans être exhaustif:

* loopback NAT (https://www.antre2geek.fr/index.php?article7/orange-et-auto-hebergement-round-2-ready-fight, https://blog.seboss666.info/2020/12/cette-livebox-est-vraiment-bonne-a-jeter-loperateur-avec/ , ) 
Sur certaines box, il n’est pas possible de pinger notre propre adresse IP externe lorsqu' on est derrière le modem. Les services ne sont donc pas disponibles lorsqu' on est connecté sur le réseau interne (quel comble), obligeant à utiliser un DNSmasq sur le réseau interne. C’est le cas avec la livebox 3, corrigée sur la livebox 4 (même si il semble qu’une mise à jour de ces dernières fasse remonter le problème). Cela impacte donc certains utilisateurs, même en 2021.

#### conclusion:
Les matérielles est pensé pour un usage familial d’une connexion internet, l’auto-hébergement y est possible, même si des contraintes techniques liée aux matérielles imposé par le fournisseur peuvent le rendre difficile voir l’empêcher dans certains cas. 

 
### Quid du débit ?

TODO




## 2 - Bare Metals Owners

## 3 - Bare Metals Users,Cloud IaaS, Paas 




