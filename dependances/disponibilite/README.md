# Axes des Disponibilité

## 1 - la typologie des disponibilité:

Lire ou se réferer à la [typologie](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/dependances/disponibilite/typo.md)

## 2 - Etudes des dépendances pour chaque type d'hébergement

L'étude pour chaque type d'hébergement est represénter par un fichier dans ce [dossier](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/tree/master/dependances/disponibilite/infra)


