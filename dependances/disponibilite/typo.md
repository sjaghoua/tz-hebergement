# Disponibilité

Une infrastructure héberge des services qu’elles exposent sur un réseau pour des utilisateurs. On parle d’indisponibilité lorsque les utilisateurs ne peuvent pas utiliser les services pour des raisons techniques, par exemple les utilisateurs ne reçoivent des erreurs 404 en essayant de se connecter aux services ou quand certaines fonctionnalités ne fonctionnent pas ou subissent des latences conséquentes.




