# TZ TSH Héberger, s’héberger, se faire héberger : externalisations et dépendances


## Comment lire le projet ?

Le projet définit plusieurs typologies et les utilise pour traiter les questions de dépendances selon plusieurs axes indépendants.


Le projet peut se lire de façon quasi linéaire.


### 1 - Introduction : 
Lire  l'[introduction](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/intro.md)

### 2 - Les typologies :
  
  * Lire et se référer en cas de besoin aux typologies suivantes pour comprendre la méthodologie:

    * les critéres et niveau de [dépendances](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/typologie/dependances.md)
    * Définition de l'[impact](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/typologie/impact.md)
    * Définition des [probas](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/typologie/proba.md)


  * Typologie des hébergement:
    Lire [Typologie](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/typologie/hebergement.md)

### 3 - Les axes de dépendances :

On peut lire chaque axe de dépendances de façon indépendantes et non linéaire.

* Lire l'axes des [backups](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/dependances/backup/README.md)

* Lire l'axes de la  [Disponibilité](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/dependances/disponibilite/README.md)

#### b - Disponibilité


### 4 - Conclusion :

Lire la [Conclusion du projet](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/blob/master/conclusion.md)



## Pour observer l'évolution du projet au cours du temps :

## Liste des rendus de la TZ: 

* [Livrable 1](https://pad.picasoft.net/p/Tz-p21-hebergement-l1)
* [Livrable 2](https://pad.picasoft.net/p/Tz-p21-hebergement-l2)
* [Livrable 3](https://pad.picasoft.net/p/Tz-p21-hebergement-l3)
* [Livrable 4 et 5](https://pad.picasoft.net/p/Tz-p21-hebergement-l45)
* [Livrable 6](https://pad.picasoft.net/p/Tz-p21-hebergement-l6)
* [Livrable 7](https://pad.picasoft.net/p/Tz-p21-hebergement-l7)
* [Livrable 8](https://pad.picasoft.net/p/Tz-p21-hebergement-l8)
* [Livrable 9](https://pad.picasoft.net/p/Tz-p21-hebergement-l9)
* [Livrable 10](https://pad.picasoft.net/p/Tz-p21-hebergement-l10)
* [Livrable 11](https://pad.picasoft.net/p/Tz-p21-hebergement-l11)
* [Livrable 12](https://pad.picasoft.net/p/Tz-p21-hebergement-l12)

## Historique du GIT sur la branche master:

Observer le [graph des commits](https://gitlab.utc.fr/sjaghoua/tz-hebergement/-/network/master)




## Comment étendre le projet ?:

Le projet peut être étendu en ajoutant des nouveaux axes.

Chaque axe est représenté par un sous dossier du dossier dépendances.

    ├── dependances 
    │  ├── backup
       ├── disponibilite 
    │  └── ...
    └── typologie 


 

La structure pour les deux premiers axes :

#### backup 
    
    ├── infra 
    │  ├── IH_a.md
       ...  
    │  └── PaaS_p.md
    └── typo.md (ce fichier définie des termes spécifiques aux backups) 

#### disponibilite 
    
    ├── infra
    │  ├── IH_a.md
       ... 
    │  └── PaaS_p.md
    ├── réseaux.md (ce fichier est une apparté sur les réseaux et l'auto hébergement, il est repris dans le dossier infra
    └── typo.md (ce fichier définie des termes spécifiques à l'axe des disponibilité) 


De façon générale chaque nouvel axe doit suivre l'architecture suivantes:


#### axe 
    ├── infra
    │  ├── IH_a.md
       ... 
    │  └── PaaS_p.md
    └── typo.md (ce fichier définie des termes spécifiques à cette l'axe) 

Notez que les fichiers du dossier infra doivent tous avoir la même structure interne.







