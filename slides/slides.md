class: center, middle

# Héberger, s’héberger, se faire héberger : externalisations et dépendances 


![](images/logo-utc.jpg)


Sami Jaghouar

Encadrant : Stéphane Crozat


---

class: middle
# Sommaires:

1. Introduction et problématisation 

2. Présentation de la méthode et des typologies

3. Applications aux backups:
  
  * IH_a
  
  * PaaS_p

4. Conclusion

---

class:  middle
# Introduction

* Héberger des services informatiques sur internet est aujourd'hui un enjeu, que ce soit dans le monde professionnel ou associatif

* Le spectre des infrastructures s'étend de l'auto-hébergement amateur chez soi jusqu'aux solutions cloud.

* Plusieurs acteurs interviennent pour mettre à disposition ces services

* Des dépendances se créée

---
class:  center, middle
# Problématique

Comment décrire et quelles sont les dépendances d’une infrastructure informatique qui héberge des services sur internet ?

---
class:  middle
# Méthode

* On effectue une grande disjonction de cas   

* Pour ce  on définit deux typologies que l'on va croiser:
  * Hébergement

  * Dépendances

* Pour chaque cas on identifie et explicite les dépendances

---
class:  middle
# Typologie des hébergement

Disjonction Professionnel/Amateur
 
* In-House (IH)

* Bare Metals Owner (BMO)

* Bare Metals Users (BMU)

* Infrastructure as a Service (IaaS)

* Platform as as Service (PaaS)

---

class:  middle
# Typologie des hébergement


![](images/diagram-typo-hebergement.png)



---
class: middle
# Typologie des axes de dépendances

Cinq axes:

* Humain

* Software

* Hardware

* Réseaux

* Cloud Provider

---
class: middle
# Dépendances/Probabilité/Impact

Pour chaque cas on applique la méthode suivante,

estimer: 
 * le niveau de dépendances à un axe,

 * la probabilité d'occurrence d'un incident 

 * l'impact d'un tel incident

---
class:  middle
# Applications aux backups

Plusieurs types de backups:

* Redondance (R)

* Interne (I)

* Déporté (DP)


---
class:  middle
# In-House amateur (IH_a)

L'infra est hébergée dans un appartement:

  * derrière une box d'un grand opérateur. 

  * Elle est composée de deux RaspberryPI (R1 et R2) chacune disposant de deux disques durs

---
class:  middle
# In-House amateur (IH_a), Backup Interne
Le backup interne est automatisé par un script shell avec une tâche cron qui copie des fichier avec rsync.

## Axes de dépendances (Dépendances/Proba/Impact):

* Humain : A/B/B
* Hardware : B/A/C
* Software : C/B/C

---
class:  middle
# PaaS Professionnel (PaaS_p)

* L'infra est hébergé sur un cloud international, 

* les services sont exposés sur un service de kubernetes managé,

* le cluster est composé d’une dizaine de VMs.

* Les données des applications sont hébergées sur des volumes et sur des bases de données managé 

---
class:  middle
# PaaS Professionnel (PaaS_p)

Ces backups correspondent aux backups des services que le cloud provider fournit

* Elles se font de façon automatique
* On peut les restaurer à tout moments

## Axes de dépendances (Dépendances/Proba/Impact):

* Humain : X/X/X
* Hardware : X/X/X
* Software : A/C/A
* Cloud : A/C/A

---
class:  middle
# Conclusion

* Dans cette étude on a mis en place une méthodologie pour décrire les relations de dépendances entre une infrastructure informatique et ses différents acteurs.

* On a appliqué cette méthodologie à l'étude des backups, et de la disponibilité.

* Autres axes possible :

  * Sécurité
  * Scalabilité
  * Réduction des coûts
  * confidentialité
