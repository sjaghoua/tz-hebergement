# Quelques exemples d'infras

TODO:

transformer plus rigouresement ces exemples en typographie

pour croiser avec la typologye des services:

qu'est ce qui pourrait causer :

* l'indisponibilité des svc
* perte de données des app
* perte des backups

chiffer les risques et en fonction des types d'infra


travailler sur les couts ?

## Infra complétement auto hébergé:

### type de svc 

petite infra, quelques services pour peux d'utilisateur (disons de 1 à 10), des clouds maisons(type google drive avec du nextcloud ou autre)
==> quelques petits services (voir par exemple https://home.samsja.fr)




### type d'infra

Réseaux : Connection internet publique, au sens d'un abonnement fibre classique avec une adresse ip qui peut changer et un routeur d'entrée type livebox ou autre 

Local : chez un particulier, dans la chambre ou dans le salon

Type de machines: Ex:

  * Raspberry pi (ou autre micro ordinateur ARM) solo ou en cluster + des racks de disque dur
  * Tour classique d'ordinateur (je dirais moins commun à cause du bruit et de la chaleur)
  * boxe spécialisé pour l'hébergement in-house, peu de bruit avec rack DD intégré
  * Une entité de machine de calcul, gros CPU + GPU + SSD, qui n'est allumée que pour les calculs combinée avec les trois options précedentes


Gérer par : une seul personne, le proriétaire des machines et du lieu d'hébergement 

### Dépendances :

* Fournisseur internet :,
  * il ne garantie pas forcément une IP fixe tout au long de l'années, 
  * sensible au débit d'un client "normal", on aura jamais les débits du cloud ou d'un datacenter  
  * sensible au panne d'internet qui peuvent être plus fréquente
  * sensible à l'activité sur le réseau local

* Logement + electricité : problème en cas de :
  
  * démenagement, cambriolage, feux, dégats des eaux, coupure électrique
  * manipulation accidentel, par un tierce par exemple

* La plus grosse dépendances dans ce type d'infra : la personne qui gére le projet:

tout repose sur une même personne, qui est souvent le principale utilisateur, qui occupe l'appartement, souscris l'abonnement internet, achètes les machines ... 
Et surtout s'occupe de tout la configuration des machines, même si il documente tout personne n'est derrière pour reprendre le projet en cas de pépin


## Infra Type entreprise/asso complétement dans le cloud

### type de svc

#### internes (en vrac):
  * mail
  * monitoring
  * drive interne
  * LDAP / SSO
  * ...

#### Externes :
  * les services développé par la boite/asso
  * Forum
  * wiki
  * ...


### type d'infra 

Tous hébergé sur un cloud international (gcp , azure, aws)

Cluster de dizaines de machines deployant des applications conteneurisé (orchestré par k8s par exemple)

Base de données managé

Utilisation de bucket pour les stockages de donées, backup automatique par le fournisseur cloud

Uilisatio des serices de CI du cloud provider 

Service propre au cloud :

ex : Big querry, lambda function, autoML ...




Géré par : Une équipe de sys admin/DevOps 2 à 3 personnes


### Dépendances :

le fournisseur cloud :.

* au niveau des prix, lorsque il est difficile de migrer l'infra, le cloud provider gère les prix comme il veut, mais les stratégies des clous providers sont souvent
d'augmenter petit à petits, de fournisse des services gratuits aux départs de les faires payer petit à petit tout en rendant l'infra dépendantes des outils

* On n'a accès à rien si il se passe quelquechose chez le fournisseur (cas OVH)

* On ne sait pas qui peut accéder aux machines et aux données

* l'activité peux être surveiller par le cloud provider comme il le veut

* Dans la mesure ou c'est une relation commercial le cloud provider peut stoper l'infra, ex, une boite qui aurait une polémique autour de ses services comment être sur 
que le cloud provider ne va pas couper ses relations avec ces entreprises pour ce dédouaner.


De l'équipe qui gérent l'infra :

minime par rapport à ce que ça pourrait être dans une asso, tout devrait être pensé pour remplacer les ingénieurs travaillant dessus, d'en ajouter et former des nouveaux
mais il y a quand même des savoires faires dans l'équipe qui peut rendre difficile d'imaginer l'infra sans eux.

mais il y a quand même des savoires faires dans l'équipe qui peut rendre difficile d'imaginer l'infra sans eux.

...


## Infra type Asso hébergé chez un hébergeur indépendant (type picasoft):

### type de svc:

#### interne
* monitoring 
* outil de dev/orga interne

#### externe
* forum
* wiki
* cloud
* service pour d'autre asso etcc ...
* ...

service communautaire similaire à ceux de l'exemple précèdent.

Moins orienté client + communauté


### type d'infra 

Plusieurs machines hébergé chez un fournisseur assosiatif (2/3 grosses machines quel dimension ?)

Plus proche de l'hébergément in-house que du full cloud, même si pas d'accès direct aux machines, tout reste géré "à la main" pas d'option de backup automatique,
on ne peut pas spammer de nouvelles machines à la demande, on ne peut pas augmenter le stockage en un clique etcc ...

* ressource limité ( pas de scalabilité immédiate)
* pas de snapshot de VM qui permetrait de remonter la machine autre part (en cas d'incident : machine defectueuse etcc ...)

on ne loue pas des VMs mais on à accès à des machines : on gére à la fois les machines host et la virtualisation 

Backup chez les membres ou chez d'autres fournisseur


Qui gère l'infra : 2-10 membres (étudiant/proffesseur) 
* pas à temps plein ( cas incident en période d'exam ? )
* équipe composé de débutant mais aussi de vétérant. 
* Cycle inévitable au niveau de l'équipe du aux 5 ans d'étude

### Dépendances :

* à l'hébergeur associatif : moins fiable qu'un cloud ? 

* aux machines, contrairement à un hébergement dans le cloud pas moyens de changer rapidement de VMs si une d'elles tombent en panne

* à l'équipe qui gère l'infra

* aux backup externes




## Infra type Asso hébergé chez dans le cloud (type scenari):

### type de svc:

#### interne
* monitoring 
* cloud 

#### externe

* My scenari
* forum
* wiki
* service de feedbqck

Plus orienté client que chez dans le cas précedent. Ils hébérgent du contenue pour des utilisateurs (aussi menbre de l'asso)


### type d'infra 

Des machines hébergé dans un coud pas forcément international.

Ils louent principalement des VMs et de l'espace de stockage

précisément :

{ 64 giga ram 
8 processeurs
2 To raid 
} x 2

Qui gère l'infra : une poignée de menbre 
* pas forcément à temps plein 
* proffesionnels/passioné 

### Dépendances :

* Le cloud 
* à l'équipe qui gère l'infra

