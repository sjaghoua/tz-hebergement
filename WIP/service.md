## Type de service



















# Typologie des services 


TODO:

à croiser avec les critères suivant (en fonction d'interne externe):

* impact d'une indisponibilité (sans perte de données)
* impact d'une perte des données 
* impact d'une perte des données + des backups



##services orientés utilisateurs non producteurs de données 

#### externe :

* site web simple, pas d'accueil
* maquette/prototype
* wiki
* organigrame

#### interne:

* monitoring visualisation (type grafana)
* CI/CD 


##services orientés utilisateurs non producteurs de données avec calcul important 
* pipeline de transformation de données (in/out) + entrainement d'algo
 
... a préciser

##services gestion de contenus mais pas de données (peertube)
besoin d'explication

##services producteur de données 

#### interne:

* montiring :
 * aggrégation des logs/metrics

#### externes:

* chat (mattermost)
* pad, kanban , ...
* forum
* my scenari





