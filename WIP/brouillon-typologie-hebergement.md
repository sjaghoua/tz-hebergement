# Typologie des hébergement

On va  ici définir une typologie des possibilités pour héberger une  infrastructure soutenant le déploiement d'une ou plusieurs applications/services. On va s'attarder en particulier à préciser et différencier les possibilités d'hébergement in-house, et les hébergements cloud. 

On ne parlera pas des possibilité d'exposer du contenue sans avoir à gérer une infrascture, par exemple via [wordpress.com](https://scenari.org/co/myScenari.html) ou [MyScenari](https://scenari.org/co/myScenari.html), qui restent tout de même des solutions fiable, performante et simple d'accès.

L'objectif de cette typologie est de la croiser avec la typologie des services, pour en dégager des solutions pragmatiques aux problématiques de dépendance, de sécurité, de coûts ( liste pas encore exhaustive).



## hébergement In House 

On appelle hébergement in house, les infrastructures entièrement gérées et maintenues par la société ou association voulant exposer des services.

### Qu'est ce qui composent un hébergement in house ?

Une infrastructure est principalement composée d'ordinateurs exécutant et exposant les services.

Dans sa forme la plus abstraite une infrastructure contient :

*  Un ou plusieurs unité de calcul contenant des  processeurs et de la RAM
* Des espaces de stockages
* Une interface réseau connectée au réseau sur lequel on veut exposer l'application.

Pourquoi séparer processeurs,ram, stockages, et ne pas juste dire qu'une infrastructure est composée d'ordinateurs ?
On veut une définition la plus générale possible. C'est notamment important lorsqu' on va hériter de cette définition pour définir certains hébergements cloud ou on ne loue pas des machines mais des processeurs virtuels et une quantité de RAM.
On ne veut aussi pas avoir à distinguer dès maintenant les architectures monolithiques et les clusters de machines.


### Exemple d'hébergement in house.

Une infrastructure peut être aussi simple qu' un ordinateur connecté à un réseau. Une option viable et souvent utilisée pour les solutions d'auto hébergement pour les particuliers est l'utilisation d'une ou plusieurs raspberry pi qui ont l'avantage d'être peu chers, et ne consomme que très peu sans prendre beaucoup de place.

### Contrainte des hébergements in-house. 

Au-delà de cette abstraction, gérer une infrastructure in house est très complexe et fait intervenir beaucoup d'autres facteurs et composants, qui rendent souvent difficile pour des petites équipes de gérer de telles installations.

D'un point de vue hardware, il faut gérer l'emplacement, la ventilation, le câblage etc. Maintenir les machines changer/réparer celles qui tomberaient en panne etc ...

D'un point de vue software, il faut mettre une virtualisation (si nécessaire), installer les OS, les maintenir à jour, sécuriser les accès ...

Il faut aussi gérer les backups en mettant en place une politique de sauvegarde.

Note: je ne pense pas que c'est ici qu'il faut rentrer de les details des avantages/inconvénient du in-house vs cloud, je ne fait que lister les points qui peuvent justifier les passages aux solutions clouds pour garder une cohérence dans l'enchaînement de la typologie.


## Bare Metals

Le terme bare metals, ou serveur dédié;  fait référence aux types d’hébergement ou l’on administre un datacenter entier. Avec un accès direct aux machines. Contrairement à l'hébergement in house, on ne gère pas le datacenter en lui même.




## Hébergement cloud

L'hébergement cloud référence tous les types d'hébergements ou un tiers prend en charge une partie plus ou moins importante de la gestion de l'infrastructure. En général, on parle de service cloud lorsque le service tiers maintient des datacenters soutenant les infrastructures.

(Lorsque les machines sont seulement localisé chez un fournisseur tierce mais ne sont pas maintenue on parlera tout de même d'hébergement in house)

De façon générale, les offres cloud fonctionnent sur le modèle suivant : On accède des ressources par le biais d'une interface et on paye proportionnellement à ce qu'on a consommé.


### IaaS (Infrascture as a Service)

C'est la solution cloud la plus proche de l'hébergement in house. Le fournisseur cloud s'occupe des serveurs, des services de virtualisation, du stockage, du réseau.

En pratique selon revient à avoir accès à une interface qui permet de louer une machine bien dimensionné, l'instancier avec un système d'exploitation adapté et s'y connecter pour l'administrer.

On est donc très proche de l'hébergement in-house, sur le déploiement des applications. On garde les mêmes problématiques de maintenance software au niveau de l'os et autres.


### PaaS (Platform as a Service)

Ici le cloud provider gère à la fois le côté hardware mais aussi une partie du software. On accède aussi à l'outil par le biais d'une interface.

Prenons un exemple pour illustrer le PaaS : une base de données managée. Une base de données est un service( interne) comme un autre qui peut être déployé sur une machine. Cependant c'est une brique critique de nombreuses autres applications. L'indisponibilité de la base de données peut entraîner l'indisponibilité de toutes les applications exposées qui s'appuient dessus. Les cloud providers proposent donc des base de données qu'ils gèrent eux mêmes, sur leurs VMs, avec leur software. Ils exposent une interface qui permet à l'utilisateur de s'y connecter et de plus ou moins configurer la base. On ne s'occupe plus des problématiques de mise à jour de load balancing , de backup ,etc pour se focaliser sur la gestion de la base, des utilisateurs, de droits ... .

TODO  : Les PaaS qui facilitent le déploiement d'application

* k8s as a service
* serverless et lambda function

## Conclusion

Pour résumer, pour bien comprendre la différence entre les hébergements in house et sur le cloud il faut bien comprendre les différentes couches de l'architecture d'une infrastructure. Comme sur le diagramme ci-dessous, on part des des problématiques physiques, gestion de locaux et du hardware, et petit à petit on arrive à la couche haute, la ou l'on déploie le code (ou l'exécutable) de notre application. Les cloud provider on exactement les mêmes problématiques (à une autre échelle cependant) que celle d'une infra in-house, mais elle fournit à ses clients la gestion d'un nombre plus ou moins important de  ces couches comme on peut le voir sur l'axe de droite sur le diagramme. Ces cloud providers ont un intérêt pour des entreprises/associations car elles garantissent le maintien de certaines de ces couches.

On peut donc voir la comparaison cloud vs in-house plutôt comme une réflection aux niveaux de ses couches. Et le choix pour une entreprise ou une association doit plutôt s'effectuer en prenant en compte cette hiérarchie de couches et non pas se contenter d'opposer cloud et in-house.

![image](images/diagram-typo-hebergement.png "diagram de la typologie d'hébergement")  


## Typologie Pratique: 
à différencier pro/amateur 

### hébergement in house (datacenter owner) 
Utilisation d'un datacenter personnel 


Dépendances :
* entretien et aménagement du datacenter/in-house : (ventilation, câblage, etc)
* aux hardwares, une panne entraîne une maintenance (pas de changement à la volée)
* aux softwares, gestion direct de OS et si besoin de la virtualisation
* à la gestion complète des solutions de backups

Dépendances cas Pro:
* à l’équipe qui va configurer tout le datacenter. 


Dépendances cas Amateur :
*  Fournisseur internet non pro (voir in-house.md)
* la personne qui gère le projet, tout repose sur elle


### Bare metals owners 
Propriétaire de machines hébergées dans un datacenter 

Dépendances :
* aux hardwares, une panne entraîne une maintenance (pas de changement à la volée)
* aux softwares, gestion direct de OS et si besoin de la virtualisation
* aux datacenters hébergeant les machines.
* à la gestion complète des solutions de backups



### Bare Metals Users 
louer des machines dédiées dans un datacenter  

Dépendances :
* aux softwares, gestion direct de OS
* à la virtualisation
* à la gestion partielles des solutions de backups : 
    * orchestration des backups
    * maintenance de l’infra de backups


* au cloud provider qui fournit le service de Bare metals:
    * à l’état du datacenter (voir feu chez ovh)
    * à la maintenance des machines que l’on loue
    * a la maintenance des machines + DD de backups







### IaaS users 
Utiliser un service de virtualisation dans un datacenter partagé 

Dépendances :

* à la gestion partielles des solutions de backups : 
    * l’orchestration des backups

* aux softwares, gestion direct de OS

* au cloud provider qui fournit le service de virtualisation (IaaS):
    * Au fonctionnement du datacenter (maintenance des machines que l’on loue + virtualisation + stockage des backups (snapshot ) )

    * A l’interface de configuration des VMs, c’est une interface supplémentaire qui est propre à chaque fournisseur (cela créer une dépendances lorsqu’on commence à scripter la gestion des VMs etcc)

    * Aux services de stockage des backups,  type glacier etcc.





### PaaS users 
Utiliser des offres clouds

* au cloud provider qui fournit le service de PaaS:
    * de sa gestion de l’infra qui supporte les services qu’ils proposent.
    * on ne connaît pas l’état de fonctionnement interne des services, pas de logs, peu de monitoring possible.
    * aux backups de l’état des services du PaaS

* aux interfaces des services qu’ils nous proposent. Même dans le cas ou le service gérer est un open source (pgsql,k8s, …) il y a toujours une interface propres aux cloud provider.



    





