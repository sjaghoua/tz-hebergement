# Dépendances aux backups:


A - Ultime
B - Forte
C - légère
D - négligeable
vide - inexistante

## 1 - a hébergement In House amateur

On va différencier plusieurs cas

c’est le cas d’une infra hébergée dans un appartement. Composé de une ou plusieurs raspberry pi et de plusieurs disques durs. l’infra est entretenue par une seule personne qui est aussi celle qui vit dans l’appartement. Les services sont hébergés pour l'utilisation personnelle de celui qui les hébergent. 

Comment sont gérées les backups ? 

Les backups sont automatisés par un petit script shell avec un cron jobs qui tournent sur les machines de l’infra. Les fichiers de sauvegarde des services sont sauvegardés en utilisant des outils comme rsync ou autre sur un ou plusieurs disques durs uniquement utilisés pour les sauvegardes. Les backups ne sont pas importants, mais de la perte de données n'entraîne pas forcément la fin de l’activité et les backups n’ont pas besoin d’être chiffré.

Les dépendances d'une telle infrastructure aux niveau des backups.


* Celui qui gère les backups l’infra : B

Même si les backups se font de façon automatique. Il y a une forte dépendance à celui qui détient le savoir sur cette orchestration. Une sauvegarde que l’on ne sait pas restaurer est aussi utile que de ne pas avoir de sauvegarde.

* Aux hardwares des noeuds de backups : B

Les disques durs de sauvegarde sont centralisés ( et c’est encore pire si il n’y en a qu’un seul). Une panne hardware (i.e une défaillance des disques dur)  sur l’un des nœuds de backup entraîne de la perte de données sauvegardé pour plusieurs services (voir pour tous les services), mais empêche aussi . 
Il est peut-être plus rare que trois disques tombent en panne la même journée, mais même dans le cas où il y a plusieurs nœuds de stockage, le fait que les disques durs soient centralisés entraîne une forte dépendance aux niveaux des backups. Par exemple en cas d'incident ou de dégâts des eaux et c’est justement dans ces cas qu’on l’on aimerait avoir des backups fonctionnelles pour tous remonter.


* Aux softwares de backups : D

Les disques durs de sauvegarde sont reliés des machines ou sont exécutés du code qui vont gérer les sauvegardes. Dans le cas des sauvegardes avec rsync, ce code est “simplement” l’os est le système de fichiers des machines. Il reste une dépendance puisque ce logiciel est une interface essentielle pour accéder aux sauvegardes mais aussi pour les orchestrer. Dans le pire des cas, un OS mal configuré ou contenant des failles ou des bugs pourrait corrompre les sauvegardes. Cette dépendance est légère puisque conceptuellement le hardware est séparé du software qui l’utilise, est qu’un software défectueux n'entraîne pas une perte de données mais une latence dans l’accès (il faut changer/réparer le software) sauf bien sûr en cas de corruption. 


## 1 - b  In House Professionnels

C’est le cas d'une entreprise qui possède son propre datacenter. Qui hébergent plusieurs gros services pour ses clients mais aussi des services en interne. Qui ne peut pas se permettre de perdre des données et a besoin de les chiffrer. Il y un tout un processus industriel (à l’inverse du cas amateur) pour orchestrer les sauvegardes mais aussi pour monitorer son bon fonctionnement. Cette orchestration est donc un travail d’équipe et plusieurs personnes sont responsables de gérer ce processus de sauvegarde

 * Ceux qui gèrent les backups : C

La dépendance à l’équipe (dans sa globalité) qui est responsable des backups est forte, mais est faible individuellement, puisque justement dans ce cas les savoirs sont partagés dans l'entreprise.

* Aux hardwares des noeuds de backups : A

C’est le nerf de la guerre pour les backups ( qui sont rappelons le essentiel au bon fonctionnement de l’entreprise). Chaque panne hardware entraîne (probablement) de la perte de données (à moins de dupliquer). Puisque ces machines de backups sont centralisées, une défaillance globale du hardware qui soutient cette orchestration de backups est dramatique. 

* Aux softwares de backups : B

Dans ce cas, les backups sont orchestrés par des outils spécialisés qui se chargent aussi du chiffrage ( et déchiffrage )  des backups. C’est donc aussi un point central pour les backups. C’est une forte dépendance. On devient dépendant des choix des développeurs

  * sur l’interface (API) pour utiliser le software, si les devs décident de rendre obsolète certaine façon d’utiliser l’outil de backup , cela implique une refonte des processus qui se base dessus.


  * Sur la sécurité, les choix techniques de chiffrages, la réactivité en cas de faille de sécurité etcc.
Dans le cas de soft propriétaire on devient encore plus dépendant aux développeurs de ces outils puisque l’on a pas la main sur les sources du projets. Il y a donc une dépendance vis à vis de l'éditeur logiciel très forte pour les backups (notamment financière mais on en parlera dans la partie appropriée).


 







## 2  ( a et b) 
Ce cas est sensiblement le même que le cas précédent au niveau des backups.




##  3 - a Bare Metals Users amateur

Dans ce cas, on loue des machines physiques dans un datacenter. Les besoins de backups sont plus importants que dans le cas 1 -a mais peut être moins important que les cas professionnels. Même s' il est impératif de ne pas perdre des données de backups, le temps pour les restaurer est moins important, on est moins dans la réactivité que pour les cas pro. La façon de sauvegarder les backups est un mixe entre le rsync du cas 1-a et l’utilisation d’outils d’orchestration de backups de 1-b. Les connaissances sur les backups sont partagées, (contrairement a 1-a) mais peut être pas autant que dans le cas pro, seul deux ou trois personnes.

* Ceux qui gèrent les backups : B

Même si le savoir est partagé c’est entre peux de personnes

* Au fournisseur de hardware des noeuds de backups : B

La dépendances aux hardwares n’existe plus puisque que l’on est plus responsable de maintenir, entretenir et remplacer les disques de backups. Mais elle se transforme directement (ou plutôt se cache) derrière une ultime dépendance aux fournisseurs de ce hardwares. On devient aussi très dépendant des choix du fournisseur pour entretenir nos machines, s' il attend qu’elles tombent en panne pour les remplacer ou bien prévoir à l’avance de les remplacer etc … . On est aussi dépendant des choix des fournisseurs dans le datacenter. Par exemple dans le cas de l’incident chez OVH. Un incendie ravage une partie du datacenter, Or certaines infra était hébergées au même endroit que leur backup. Entraînant une perte totale de données et donc la fin de certaines activités amateur et pro !



* Aux softwares de backups : C

Intermédiaire entre 1-a et 1-b. 



## 3 - b  Bare Metals Users pro

Ce cas reprend le cas amateur mais y ajoute une plus grande importance aux backups et une gestion plus industriel de celle ci (comme dans le cas 1-b)

* Ceux qui gèrent les backups : C
* Au fournisseur de hardware des noeuds de backups : A
* Aux softwares de backups : B


## 4 - a  IaaS amateur

On utilise que des machines virtuelles et on a donc pas d’accès direct aux machines. Les backups dans le cas amateur sont géré comme dans le cas precedents (3-a) avec les mêmes importances. La seule différence est que l’on passe par l’interface du cloud provider pour gérer et accéder aux machines de backups.

* Ceux qui gèrent les backups : B
* Au fournisseur de hardware des noeuds de backups : B
* Aux softwares de backups : C

* A l’interface pour accéder aux machines : B

On a pas d’accès physique aux machines, mais seulement à des VMs qui montent virtuellement des volumes soutenus physiquement par des machines équipées de disques durs. Ainsi on est très dépendant, lors des processus de backups, de cette interface. (A PRECISER)

## 4 - b

* Ceux qui gèrent les backups : C
* Au fournisseur de hardware des noeuds de backups : A
* Aux softwares de backups : B
* A l’interface pour accéder aux machines : B+

## 5 -a : PaaS Amateur

Dans les cas PaaS on héberge quelques services tous très fortement soutenus par des outils du cloud provider. On héberge par exemple, une instance de Mattermost (chat opensource type slack) via un Kubernetes managé, les données stockées sur une instance SQL managé. En ce qui concerne les backups tout est automatique, c’est le cloud provider qui fournit le service de SQL qui s’occupe de backuper les données et de mettre à notre disposition une interface pour, en cas de problème, restaurer une backup.

On peut sauvegarder d’autre type de données dans des instances types glaciers ou sur des bucket cloud storage comme on le ferait sur un drive.

* Ceux qui gèrent les backups : D

La dépendance aux personnes qui s’occupe de faire les backups est négligeable, car en cas de problème ( celui qui sait comment restaurer les backups n’est plus disponible) les services clients du cloud peuvent facilement aider. (A DISCUTER)

* Au cloud provider : B

Toutes les backups sont géré par le cloud provider de A à Z, on décide juste de quand on veut restaurer.

## 5 - B

C’est la même chose dans le cas pro, c'est-à dire que on ne peut pas perdre de backups.

* Au cloud provider : A
* Ceux qui gèrent les backups : D-








## Conclusion



|     | Equipes | Hardware  | Hardware Cloud | software | Interface d'accès cloud | Backups Cloud |
|-----|---------|-----------|----------------|----------|-------------------------|---------------|
| 1-a | B       | B         |                | D        |                         |               |
| 1-b | C       | A         |                | B        |                         |               |
| 2-a | B       | B         |                | D        |                         |               |
| 2-b | C       | A         |                | B        |                         |               |
| 3-a | B       |           | B              | C        |                         |               |
| 3-b | C       | A         |                | B        |                         |               |
| 4-a | B       |           | B              | C        | B                       |               |
| 4-b | C       |           | A              | B        | B+                      |               |
| 5-a | D       |           |                |          |                         | B             |
| 5-b | D-      |           |                |          |                         | A             |

